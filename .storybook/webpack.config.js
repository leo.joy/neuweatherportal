const path = require("path");
const SRC_PATH = path.join(__dirname, '../src');
const STORIES_PATH = path.join(__dirname, '../stories');
//dont need stories path if you have your stories inside your //components folder
module.exports = ({ config }) => {
    config.module.rules.push({
        test: /\.(ts|tsx)$/,
        include: [SRC_PATH, STORIES_PATH],
        use: [
            {
                loader: require.resolve('awesome-typescript-loader'),
                options: {
                    configFileName: './.storybook/tsconfig.json'
                }
            },
            { loader: require.resolve('react-docgen-typescript-loader') },

        ]
    });

    config.module.rules.push(
        {
            test: /\.scss$/,
            use: [
                "style-loader",

                {
                    loader: "css-loader" // translates CSS into CommonJS
                },
                'resolve-url-loader',
                {
                    loader: "sass-loader", options: { sourceMap: true }
                },


            ]
        },
    )
    config.resolve.extensions.push('.ts', '.tsx');
    config.resolve.extensions.alias = {

        "@neu_weather_ap/components": path.resolve(__dirname, 'src/components'),
        "@neu_weather_ap/store": path.resolve(__dirname, 'src/store'),
        "@neu_weather_ap/assets": path.resolve(__dirname, 'src/assets'),
        "@neu_weather_ap/lib": path.resolve(__dirname, 'src/lib'),
        "@neu_weather_ap/pages": path.resolve(__dirname, 'src/pages'),
        "@neu_weather_ap/layout": path.resolve(__dirname, 'src/layout'),
        "@neu_weather_ap/saga": path.resolve(__dirname, 'src/store/saga'),
        "@neu_weather_ap/reducers": path.resolve(__dirname, 'src/store/reducers'),
        "@neu_weather_ap/containers": path.resolve(__dirname, 'src/containers'),
        "@neu_weather_ap/view-models": path.resolve(__dirname, 'src/view-models'),
        "@neu_weather_ap/constants": path.resolve(__dirname, 'src/constants'),
        "@neu_weather_ap/services": path.resolve(__dirname, 'src/services')

    }

    return config;
};