import React from 'react'

import { Animate } from 'react-simple-animate'

import { ResponsiveContainer, CartesianGrid, XAxis, YAxis, Legend, Tooltip, BarChart } from 'recharts'


import TrackVisibility from 'react-on-screen';

export const CustomBarChart = (props: WeatherBarChartProps) => {
    return (
        <div className="chart-container">
            <div className="city-name-text">{props.title}</div>
            <TrackVisibility offset={30} once>
                {({ isVisible }) => isVisible && <Animate
                    play={true} // set play true to start the animation
                    duration={2} // how long is the animation duration
                    delay={.4} // how many delay seconds will apply before the animation start
                    end={{ opacity: 1 }}
                    start={{ opacity: 0 }}
                    // complete={{ display: 'none' }}
                    easeType="cubic-bezier(0.445, 0.05, 0.55, 0.95)"
                >
                    <ResponsiveContainer width="100%" height={500}>
                        <BarChart
                            data={props.data}
                        >
                            <CartesianGrid strokeDasharray="3 3" />

                            <XAxis dataKey="date" />
                            <YAxis minTickGap={5} />
                            {props.children}
                            {/* <Bar dataKey="tempMin" stroke="#8884d8" fill="rgb(130, 202, 157,0.4)" />
                    <Bar dataKey="tempMax" stroke="#8884d8" fill="rgb(255, 198, 88,0.6)" /> */}
                            <Legend></Legend>
                            {/* <Area type="monotone" dataKey="tempMin" stackId="2" stroke="#82ca9d" fill="#82ca9d" />
                    <Area type="monotone" dataKey="tempMax" stackId="3" stroke="#8799d8" fill="#8799d8" /> */}

                            <Tooltip></Tooltip>
                        </BarChart>
                    </ResponsiveContainer>
                </Animate>
                }
            </TrackVisibility>
        </div>


    )
}

interface WeatherBarChartProps {
    title: string
    data: Array<any>
    children:  any
}