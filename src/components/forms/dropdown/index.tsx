
import { validate } from "../validator";
import { FormGroup, Label } from 'reactstrap';
import { useState } from "react";
import * as React from "react";
import 'react-widgets/dist/css/react-widgets.css'
import "./style.scss";
import DropdownList from 'react-widgets/lib/DropdownList'


export const useDropDownFormInput = (initialValue: any, valueField: string, textField: string, validators: Array<any>) => {
    const [value, setValue] = useState(initialValue);
    const [isValid, setIsValid] = useState(false);
    const [errorMessage, setErrorMessage] = useState("");
    const validateWidget = (value:any) => {
        let validator = validate(validators, value[valueField])
        setIsValid(validator.isValid)
        setErrorMessage(validator.message)
    }
    const onChange = (value: any) => {
        console.log(value)
        setValue(value)
        // validateWidget(value)

    }


    const widget = (label: string, placeholder: string, disabled: boolean = false, data: Array<any>) => {
        return <FormGroup>
            {label && <Label for="exampleEmail">{label}</Label>}

            <DropdownList onSelect={(s) => onChange(s)}
                data={data}
                textField={textField}
                valueField={valueField}
                filter={"startsWith"}
                value={value}

                placeholder={placeholder} />

            {!isValid && <Label style={{ color: "red" }}>{errorMessage}</Label>}
        </FormGroup>
    }


    return {
        value,
        setValue,
        onChange,
        widget,
        errorMessage,
        valid: isValid,
        validateWidget: validateWidget
    };

}
