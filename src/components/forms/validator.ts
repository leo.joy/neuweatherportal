import moment from 'moment';

const isRequired = (value: string) => {
    if (value && value.length > 0) {
        return true;
    }
    return false;
};

const requiredValidator = (message: string) => {
    const validate = (value: string) => {

        if (!value || !isRequired(value)) {
            return { status: false, message: message }

        }
        return { status: true, message: "" }
    }
    return { validate }

};

const requirePhoneNumber = (message: string) => {
    const validate = (value: string) => {

        var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
        if (value.match(phoneno)) {
            return { status: true, message: "" }
        }
        else {
            return { status: false, message: message }

        }

    }
    return { validate }

}

const requireIdCardNumber = (message: string) => {
    const validate = (value: string) => {

        var phoneno = /(^\d{4}$)|(^\d{4}-\d{3}$)/;
        if (value.match(phoneno)) {
            return { status: true, message: "" }
        }
        else {
            return { status: false, message: message }

        }

    }
    return { validate }

}
const requiredDropDownValidator = (message: string) => {
    const validate = (value: any) => {

        if (!value || !isRequired(value.value)) {
            return { status: false, message: message }

        }
        return { status: true, message: "" }
    }
    return { validate }

};

const requiredDateValidator = (message: string) => {
    const validate = (value: any) => {

        if (!value) {
            return { status: false, message: message, warning: false }

        }
        if (moment(value) <= moment()) {
            return { status: true, message: "The certificate is expired", warning: true }
        }
        else {
            return { status: true, message: "", warning: false }
        }
    }
    return { validate }

};

export const validators = {
    requiredValidator,
    requiredDropDownValidator,
    requiredDateValidator,
    requirePhoneNumber,
    requireIdCardNumber
};

export const validate = (validators: Array<any>, value: any) => {
    let isValid = true;
    let message = ""
    let warning = false
    for (var item of validators) {
        let validator = item.validate(value);
        isValid = validator.status
        message = validator.message
        warning = validator.warning

        if (!isValid) break;
    }
    return { isValid, message, warning };
};
