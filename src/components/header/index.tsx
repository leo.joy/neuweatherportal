import { Container } from 'reactstrap'
import React from 'react'
import { Animate } from 'react-simple-animate';
import { Link } from 'react-router-dom';

export const AppHeader = (props: any) => {

    return (
        <Animate play start={{opacity:0}} end={{opacity:1}} duration={1}>
            <Container fluid className="neu_header">
                <Link to="/">Neu Weather Portal</Link>
            </Container>
        </Animate>
    )
}