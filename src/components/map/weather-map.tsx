import React from 'react';
import { Map, TileLayer, LayersControl } from 'react-leaflet'
import { AppConstants } from '../../constants/app-contansts';
const { Overlay } = LayersControl;
export const WeatherMap = (props: WeatherMapProps) => {

    return <Map id="map"

        // onClick={this.onClickReset}
        // onViewportChanged={this.onViewportChanged}
        viewport={
            {
                center: [props.lat, props.long],
                zoom: 6,
            }
        }>
        <TileLayer
            attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url="https://tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png"
        />
        <LayersControl collapsed={false} position="topright">

            <Overlay name="Temperature" >
                <TileLayer
                    attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    // url="http://{s}.tile.osm.org/{z}/{x}/{y}.png"

                    url={`https://tile.openweathermap.org/map/temp_new/{z}/{x}/{y}.png?appid=${AppConstants.weatherApiToken}`}
                />
            </Overlay>
            <Overlay name="Wind" >
                <TileLayer
                    attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    // url="http://{s}.tile.osm.org/{z}/{x}/{y}.png"

                    url={`https://tile.openweathermap.org/map/wind_new/{z}/{x}/{y}.png?appid=${AppConstants.weatherApiToken}`}
                />
            </Overlay>
            <Overlay checked name="Precipitation" >
                <TileLayer
                    attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    // url="http://{s}.tile.osm.org/{z}/{x}/{y}.png"

                    url={`https://tile.openweathermap.org/map/precipitation_new/{z}/{x}/{y}.png?appid=${AppConstants.weatherApiToken}`}
                />
            </Overlay>

        </LayersControl>

    </Map>

}
interface WeatherMapProps {
    lat: number,
    long: number
}