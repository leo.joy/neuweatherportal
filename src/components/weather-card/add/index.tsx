import React from 'react'
import classNames from 'classnames'
import "./style.scss"
import { Animate } from 'react-simple-animate'

// import SVG from 'react-inlinesvg';
// const AddIcon = () => <SVG src="./add.svg" />;

export const NewWeatherCard = (props: any) => {
    return (
        <div onClick={props.onClick} className={classNames({ [props.className]: true })}>
            <Animate
                play={true} // set play true to start the animation
                duration={1} // how long is the animation duration
                delay={0.3} // how many delay seconds will apply before the animation start
                end={{ transform: 'translate(0, 0)' }}
                start={{ transform: 'translate(0, 150px)' }}
                // complete={{ display: 'none' }}
                easeType="cubic-bezier(0.445, 0.05, 0.55, 0.95)"
            >
                <div style={{
                    justifyContent: "center",
                    alignItems: "center"
                }} className={classNames({ "new-weather-card ": true })}>

                    <div className="header">
                        <span>Add city</span>
                    </div>
                    <div className="body">
                        <svg _ngcontent-mee-c3="" className="add__icon" data-name="Plus Icon" id="Plus_Icon" viewBox="1454.4 326.4 209.1 209.1" style={{ width: "10rem", marginBottom: "1.15rem" }} >
                            <defs _ngcontent-mee-c3="">
                                <style _ngcontent-mee-c3=""> .cls-299  </style>
                                <filter _ngcontent-mee-c3="" filterUnits="userSpaceOnUse" height="209.1" id="Ellipse_7" width="209.1" x="1454.4" y="326.4">
                                    <feOffset _ngcontent-mee-c3="" dy="3" />
                                    <feGaussianBlur _ngcontent-mee-c3="" result="blur" stdDeviation="3" />
                                    <feFlood _ngcontent-mee-c3="" floodColor="#333" floodOpacity=".1" />
                                    <feComposite _ngcontent-mee-c3="" in2="blur" operator="in" />
                                    <feComposite _ngcontent-mee-c3="" in="SourceGraphic" />
                                </filter>
                            </defs>
                            <g _ngcontent-mee-c3="" filter="url(#Ellipse_7)">
                                <circle _ngcontent-mee-c3="" cx="95.6" cy="95.6" data-name="Ellipse 7" fill="#f5f8ff" id="Ellipse_7-2" r="95.6" transform="translate(1463.4 332.4)" />
                            </g>
                            <rect _ngcontent-mee-c3="" className="cls-299" data-name="Rectangle 3" height="142.9" id="Rectangle_3" rx="5" transform="translate(1552.6 358.1)" width="12.8" />
                            <rect _ngcontent-mee-c3="" className="cls-299" data-name="Rectangle 4" height="142.9" id="Rectangle_4" rx="5" transform="rotate(90 603.6 1026.8)" width="12.8" />
                        </svg>
                        <svg _ngcontent-mee-c3="" className="city__illustration" data-name="City Illustration" id="City_Illustration" viewBox="1367.8 616.6 403.6 331.4">
                            <defs _ngcontent-mee-c3="">


                            </defs>
                            <path _ngcontent-mee-c3="" className="cls-1" d="M1488.3 646.3v-30H1892v30z" data-name="City grass" id="City_grass" transform="translate(-120.5 301.7)"></path>
                            <g _ngcontent-mee-c3="" id="Buidings">
                                <g _ngcontent-mee-c3="" data-name="Buiding Shadow" id="Buiding_Shadow">
                                    <path _ngcontent-mee-c3="" className="cls-2" d="M1488.3 777.8V616.3h26.6v161.5z" data-name="Path 23" id="Path_23" transform="translate(-120.5 48.4)"></path><path _ngcontent-mee-c3="" className="cls-2" d="M1488.3 788.9V616.3h26.6V789z" data-name="Path 29" id="Path_29" transform="translate(-30.5 5.3)"></path><path _ngcontent-mee-c3="" className="cls-2" d="M1488.3 783.8V616.3h96.8v167.5z" data-name="Path 21" id="Path_21" transform="translate(176.5 45.4)"></path><path _ngcontent-mee-c3="" className="cls-2" d="M1488.3 735V616.3h34.3V735z" data-name="Path 30" id="Path_30" transform="translate(128.5 48.2)"></path><path _ngcontent-mee-c3="" className="cls-2" d="M1488.3 811V616.3h34.3V811z" data-name="Path 31" id="Path_31" transform="translate(28.5 5.2)"></path></g><g _ngcontent-mee-c3="" id="Core"><path _ngcontent-mee-c3="" className="cls-3" d="M1488.3 771.2V616.3h59.7v154.9z" data-name="Path 8" id="Path_8" transform="translate(-112 54)"></path><path _ngcontent-mee-c3="" className="cls-1" d="M1488.3 825V616.2h96.8V825z" data-name="Path 9" id="Path_9" transform="translate(-43.5 .3)"></path><path _ngcontent-mee-c3="" d="M1488.3 660.7v-44.4h96.8v44.4z" data-name="Path 10" fill="#7affcb" id="Path_10" transform="translate(62.5 164.4)"></path><path _ngcontent-mee-c3="" className="cls-3" d="M1488.3 771.2V616.3h96.8v154.9z" data-name="Path 11" id="Path_11" transform="translate(168.5 54)"></path></g></g><path _ngcontent-mee-c3="" d="M1488.3 692.6v-76.3H1892v76.3z" data-name="City water" fill="#8091ff" id="City_water" transform="translate(-120.5 217.2)"></path><g _ngcontent-mee-c3="" id="Windows" transform="translate(8 40)"><path _ngcontent-mee-c3="" className="cls-6" d="M1488.3 628.5v-12.2h36.6v12.2z" data-name="Path 13" id="Path_13" transform="translate(-43.2 -19.8)"></path><path _ngcontent-mee-c3="" className="cls-7" d="M1488.3 628.5v-12.2h36.6v12.2z" data-name="Path 14" id="Path_14" transform="translate(-43.2 2.2)"></path><path _ngcontent-mee-c3="" className="cls-7" d="M1488.3 628.5v-12.2h83.7v12.2z" data-name="Path 25" id="Path_25" transform="translate(168.2 110.2)"></path><path _ngcontent-mee-c3="" className="cls-7" d="M1488.3 628.5v-12.2h83.7v12.2z" data-name="Path 26" id="Path_26" transform="translate(169.2 132.2)"></path><path _ngcontent-mee-c3="" d="M1488.3 628.5v-12.2h36.6v12.2z" data-name="Path 18" fill="#a1feda" id="Path_18" transform="translate(-108.2 26.2)"></path><path _ngcontent-mee-c3="" d="M1488.3 628.5v-12.2h36.6v12.2z" data-name="Path 20" fill="#3564fb" id="Path_20" transform="translate(-108.2 146.2)"></path><path _ngcontent-mee-c3="" d="M1488.3 665.4v-49h36.6v49z" data-name="Path 19" fill="#a2b8ff" id="Path_19" transform="translate(-108.2 54.3)"></path><path _ngcontent-mee-c3="" className="cls-11" d="M1488.3 651v-34.7h36.6V651z" data-name="Path 15" id="Path_15" transform="translate(-3.2 47.2)"></path><path _ngcontent-mee-c3="" d="M1488.3 651v-34.7h36.6V651z" data-name="Path 16" fill="#3b5dca" id="Path_16" transform="translate(-46.2 46.2)"></path><path _ngcontent-mee-c3="" d="M1488.3 651v-34.7h36.6V651z" data-name="Path 17" fill="#597ff7" id="Path_17" transform="translate(-12.2 117.2)"></path><path _ngcontent-mee-c3="" className="cls-6" d="M1488.3 641.2v-24.9h62.9v24.9z" data-name="Path 27" id="Path_27" transform="translate(169.1 28.2)"></path><path _ngcontent-mee-c3="" className="cls-14" d="M1488.3 634.3v-18h17.5v18z" data-name="Path 28" id="Path_28" transform="translate(194.4 56)"></path><path _ngcontent-mee-c3="" className="cls-14" d="M1488.3 634.3v-18h17.5v18z" data-name="Path 46" id="Path_46" transform="translate(194.4 82)"></path><path _ngcontent-mee-c3="" className="cls-14" d="M1488.3 634.3v-18h17.5v18z" data-name="Path 47" id="Path_47" transform="translate(169.4 82)"></path><path _ngcontent-mee-c3="" className="cls-7" d="M1488.3 634.3v-18h17.5v18z" data-name="Path 48" id="Path_48" transform="translate(169.4 56)"></path><path _ngcontent-mee-c3="" className="cls-11" d="M1488.3 634.3v-18h17.5v18z" data-name="Path 44" id="Path_44" transform="translate(232.4 56)"></path><path _ngcontent-mee-c3="" d="M1488.3 634.3v-18h17.5v18z" data-name="Path 45" fill="#aebff7" id="Path_45" transform="translate(232.4 84)"></path></g><path _ngcontent-mee-c3="" d="M1580.5 875.5c1-.3-17.9-39 .2-85.3h40s-11.4 42.6 5.5 85.3c-47.4-1.7-46.7.3-45.7 0z" fill="#ccc8ff" id="Bridge" transform="translate(8 40)"></path><g _ngcontent-mee-c3="" data-name="Water Turbulence" id="Water_Turbulence" transform="translate(8 40)"><g _ngcontent-mee-c3="" data-name="Group 4" id="Group_4"><path _ngcontent-mee-c3="" className="cls-17" d="M1386.3 831.3s18.2 3.5 21.8-5.2" data-name="Path 42" id="Path_42" transform="translate(11.8 2)"></path><path _ngcontent-mee-c3="" className="cls-17" d="M1401.3 831.3a23 23 0 0 1-3.7.7c-3.8.3-9.7-.2-11.3-5.9" data-name="Path 43" id="Path_43" transform="translate(32.1 2)"></path></g><g _ngcontent-mee-c3="" data-name="Group 5" id="Group_5" transform="translate(74 -14)"><path _ngcontent-mee-c3="" className="cls-17" d="M1386.3 831.3s18.2 3.5 21.8-5.2" data-name="Path 42" id="Path_42-2" transform="translate(11.8 2)"></path><path _ngcontent-mee-c3="" className="cls-17" d="M1401.3 831.3a23 23 0 0 1-3.7.7c-3.8.3-9.7-.2-11.3-5.9" data-name="Path 43" id="Path_43-2" transform="translate(32.1 2)"></path></g><g _ngcontent-mee-c3="" data-name="Group 8" id="Group_8" transform="translate(85 17)"><path _ngcontent-mee-c3="" className="cls-17" d="M1386.3 831.3s18.2 3.5 21.8-5.2" data-name="Path 42" id="Path_42-3" transform="translate(11.8 2)"></path><path _ngcontent-mee-c3="" className="cls-17" d="M1401.3 831.3a23 23 0 0 1-3.7.7c-3.8.3-9.7-.2-11.3-5.9" data-name="Path 43" id="Path_43-3" transform="translate(32.1 2)"></path></g><g _ngcontent-mee-c3="" data-name="Group 17" id="Group_17" transform="translate(315 14)"><path _ngcontent-mee-c3="" className="cls-17" d="M1386.3 831.3s18.2 3.5 21.8-5.2" data-name="Path 42" id="Path_42-4" transform="translate(11.8 2)"></path><path _ngcontent-mee-c3="" className="cls-17" d="M1401.3 831.3a23 23 0 0 1-3.7.7c-3.8.3-9.7-.2-11.3-5.9" data-name="Path 43" id="Path_43-4" transform="translate(32.1 2)"></path></g><g _ngcontent-mee-c3="" data-name="Group 18" id="Group_18" transform="translate(245 -17)"><path _ngcontent-mee-c3="" className="cls-17" d="M1386.3 831.3s18.2 3.5 21.8-5.2" data-name="Path 42" id="Path_42-5" transform="translate(11.8 2)"></path><path _ngcontent-mee-c3="" className="cls-17" d="M1401.3 831.3a23 23 0 0 1-3.7.7c-3.8.3-9.7-.2-11.3-5.9" data-name="Path 43" id="Path_43-5" transform="translate(32.1 2)"></path></g></g><g _ngcontent-mee-c3="" id="Fence" transform="translate(8 40)"><g _ngcontent-mee-c3="" id="Poles"><rect _ngcontent-mee-c3="" className="cls-18" data-name="Rectangle 13" height="25" id="Rectangle_13" rx="2" transform="translate(1375 858)" width="5"></rect><rect _ngcontent-mee-c3="" className="cls-18" data-name="Rectangle 18" height="25" id="Rectangle_18" rx="2" transform="translate(1450 858)" width="5"></rect><rect _ngcontent-mee-c3="" className="cls-18" data-name="Rectangle 21" height="25" id="Rectangle_21" rx="2" transform="translate(1525 858)" width="5"></rect><rect _ngcontent-mee-c3="" className="cls-18" data-name="Rectangle 14" height="25" id="Rectangle_14" rx="2" transform="translate(1400 858)" width="5"></rect><rect _ngcontent-mee-c3="" className="cls-18" data-name="Rectangle 17" height="25" id="Rectangle_17" rx="2" transform="translate(1475 858)" width="5"></rect><rect _ngcontent-mee-c3="" className="cls-18" data-name="Rectangle 20" height="25" id="Rectangle_20" rx="2" transform="translate(1550 858)" width="5"></rect><rect _ngcontent-mee-c3="" className="cls-18" data-name="Rectangle 15" height="25" id="Rectangle_15" rx="2" transform="translate(1425 858)" width="5"></rect><rect _ngcontent-mee-c3="" className="cls-18" data-name="Rectangle 16" height="25" id="Rectangle_16" rx="2" transform="translate(1500 858)" width="5"></rect></g><g _ngcontent-mee-c3="" id="Ropes"><path _ngcontent-mee-c3="" className="cls-19" d="M1377.5 858c.4-1 14.2 17 25.4 0" data-name="Path 45" id="Path_45-2" transform="translate(0 4)"></path><path _ngcontent-mee-c3="" className="cls-19" d="M1377.5 858c.4-1 14.2 17 25.4 0" data-name="Path 46" id="Path_46-2" transform="translate(25 5)"></path><path _ngcontent-mee-c3="" className="cls-19" d="M1377.5 858c.4-1 14.2 17 25.4 0" data-name="Path 47" id="Path_47-2" transform="translate(50 3)"></path><path _ngcontent-mee-c3="" className="cls-19" d="M1377.5 858c.4-1 14.2 17 25.4 0" data-name="Path 48" id="Path_48-2" transform="translate(75 5)"></path><path _ngcontent-mee-c3="" className="cls-19" d="M1377.5 858c.4-1 14.2 17 25.4 0" data-name="Path 49" id="Path_49" transform="translate(101 3)"></path><path _ngcontent-mee-c3="" className="cls-19" d="M1377.5 858c.4-1 14.2 17 25.4 0" data-name="Path 50" id="Path_50" transform="translate(125 7)"></path><path _ngcontent-mee-c3="" className="cls-19" d="M1377.5 858c.4-1 14.2 17 25.4 0" data-name="Path 51" id="Path_51" transform="translate(150 5)"></path></g></g>
                        </svg>
                    </div>
                </div>

            </Animate>
        </div>
    )
}

