import React from 'react'
import { Animate } from 'react-simple-animate'

export const DayWeatherCard = (props: DayWeatherCardProps) => {

    return (
        <Animate
            play={true} // set play true to start the animation
            duration={1} // how long is the animation duration
            delay={props.animationDelay+2} // how many delay seconds will apply before the animation start
            end={{ opacity:1 }}
            start={{ opacity:0 }}
            // complete={{ display: 'none' }}
            easeType="cubic-bezier(0.445, 0.05, 0.55, 0.95)"
        >
            <div className="day-weather-container">
                <span className="day-name-text">
                    {props.day}
                </span>
                <img src={`http://openweathermap.org/img/wn/${props.icon}@2x.png`} />

                <span className="day-temp-text">
                    {props.temperature}°
        </span>
                <span className="day-state-text">
                    {props.text}
                </span>

            </div>
        </Animate>
    )
}
interface DayWeatherCardProps {
    day: string
    icon: string
    temperature: string
    text: string
    animationDelay:number
}