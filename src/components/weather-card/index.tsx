import React, { } from 'react'
import classNames from 'classnames'
import "./style.scss"
import { Animate } from 'react-simple-animate'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowUp, faArrowDown, faTrashAlt } from '@fortawesome/free-solid-svg-icons'
import { CityDetails } from '../../view-models/city-model'
import { LocalStorageService } from '../../services/local-storage-service'
import { AppConstants } from '../../constants/app-contansts'

export const WeatherCard = (props: WeatherCardProps) => {
    const onDelete = (event: any, city: Partial<CityDetails>) => {
        event.preventDefault()
        event.stopPropagation()
        if (city.coord && city.coord.lat && city.coord.lon) {
            let lat = city || { coord: {} }.coord || { lat: "" }.lat
            let lon = city || { coord: {} }.coord || { lon: "" }.lon
            LocalStorageService.removeItem(AppConstants.cityDataBase, (x: any) => x.lat == lat && x.lon == lon)
        }
        if (props.removeItem) {
            props.removeItem(city)
        }
        return false

    }
    return (
        <div className={classNames({ [props.className]: true })}>
            <Animate
                play={true} // set play true to start the animation
                duration={2} // how long is the animation duration
                delay={0.3} // how many delay seconds will apply before the animation start
                end={{ opacity: 1 }}
                start={{ opacity: 0 }}
                // complete={{ display: 'none' }}
                easeType="cubic-bezier(0.445, 0.05, 0.55, 0.95)"
            >

                <div onClick={() => props.onClick(props.city)} className={classNames({ "weather-card": true })}>
                    {props.removeItem && <a onClick={(ev) => onDelete(ev, props.city)} className="float-left delete-btn "><FontAwesomeIcon icon={faTrashAlt}></FontAwesomeIcon> </a>
                    }
                    <span className="city-name">{props.city.name}</span>
                    <div className="weather-icon-container">
                        <img src={`http://openweathermap.org/img/wn/${props.city.weatherIcon}@2x.png`}
                            style={{ width: 150, height: 150 }}

                        />                </div>
                    <div className="weather-data-container">
                        <span className="current-temp">{props.city.temp}° </span>
                        <span >{props.city.weatherText}</span>
                        <div className="weather-stat">
                            <div className="weather-stat-col down">
                                <FontAwesomeIcon className="weather-icon" icon={faArrowDown}></FontAwesomeIcon>
                                <span className="temp-text">{props.city.tempMin}° </span>
                                <span className="temp-des">Min</span>
                            </div>
                            <div className="weather-stat-col up">
                                <FontAwesomeIcon className="weather-icon" icon={faArrowUp}></FontAwesomeIcon>
                                <span className="temp-text">{props.city.tempMax}° </span>
                                <span className="temp-des">Max</span>
                            </div>
                        </div>
                    </div>
                </div>
            </Animate>

        </div>
    )
}

interface WeatherCardProps {
    className: string
    title: string
    city: Partial<CityDetails>
    onClick: any
    removeItem?: any
}