export const AppConstants = {
    "baseUrl": "https://api.openweathermap.org/data/2.5/",
    "apiTimeOut": 65000,
    "weatherApiToken":"aefcdf17dc17f2b248b3fe4c687f21b3",
    "mapBoxAccessToken": "pk.eyJ1IjoibGVvam95MjAiLCJhIjoiY2s3YTlkaWRnMHh6ZTNwcjFzdGZ5eDhrMiJ9.07aovtYeMpHol4-myE5iAw",
    "cityDataBase":"city"
}