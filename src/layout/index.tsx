import { Route } from "react-router"
import React from 'react'
import { AppHeader } from '@neu_weather_ap/components/header'

export const Layout = (props: LayoutProps) => {
    return <Route path={props.path}>
        <AppHeader></AppHeader>
        {props.children}
    </Route>
}
interface LayoutProps {
    children: any
    path: string
    isAuthRequired:boolean
    showSidebar:boolean

}