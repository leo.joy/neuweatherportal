import moment from "moment";
export const MapForecastData = (data: Array<any>) => {
    return data.map(
        (item) => {
            let weather = ((item.weather || [])[0] || {})
            let tempData = (item.main || {})
            return {
                date: moment.unix(item.dt).format("DD/MM/YYYY"),
                time: moment.unix(item.dt).format("HH"),
                weatherIcon: weather.icon,
                weatherText: weather.main,
                temp: tempData.temp,
                feelsLike: tempData.feels_like,
                tempMin: tempData.temp_min,
                tempMax: tempData.temp_max,
                pressure: tempData.pressure,
                seaLevel: tempData.sea_level,
                grndLevel: tempData.grnd_level,
                humidity: tempData.humidity

            }
        }
    )
}
export function getCityDetails(data) {
    let item = (data.list || [])[0]
    let weather = ((item.weather || [])[0] || {})
    let tempData = (item.main || {})
    let wind = (item.wind || {})
    let cityData = (data.city || {})
    debugger
    return {
        name: cityData.name,
        coord: cityData.coord,
        country: cityData.country,
        weatherIcon: weather.icon,
        weatherText: weather.main,
        temp: tempData.temp,
        feelsLike: tempData.feels_like,
        tempMin: tempData.temp_min,
        tempMax: tempData.temp_max,
        pressure: tempData.pressure,
        seaLevel: tempData.sea_level,
        grndLevel: tempData.grnd_level,
        humidity: tempData.humidity,
        wind: wind.speed,
        monthlyForecasts: []
    }
}