import { ActionTypes } from '@neu_weather_ap/store/actions/action-types';

export interface ActionType{
    type:ActionTypes
    payload:any
}