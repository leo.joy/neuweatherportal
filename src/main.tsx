import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router'

import { createHashHistory } from 'history';
import Routes from './route';
import "bootstrap"
import "bootstrap/dist/css/bootstrap.css";
import "./assets/styles/app.scss"
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer, toast } from 'react-toastify';
import { configureStore } from './store';


// prepare store
const history = createHashHistory();
const store = configureStore(history);
export { store }
ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Routes />
      <ToastContainer position={toast.POSITION.TOP_RIGHT} ></ToastContainer>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
);
