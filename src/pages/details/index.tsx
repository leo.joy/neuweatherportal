import { Container, } from 'reactstrap';
import React, { useEffect } from 'react';

import "./style.scss"
import { DayWeatherCard } from '@neu_weather_ap/components/weather-card/day'
import { useParams } from 'react-router'
import { useDispatch, useSelector } from 'react-redux'
import { FetchMonthlyForecasts } from '@neu_weather_ap/store/actions/app-actions'
import { NeuWeatherAppState } from '@neu_weather_ap/view-models/app-state'
import { Bar, Line } from 'recharts'
import { uniqBy } from "lodash";
import moment from 'moment'
import { Animate } from 'react-simple-animate'
import TrackVisibility from 'react-on-screen';
import { CustomLineChart } from '@neu_weather_ap/components/charts/line-chart';
import { CustomBarChart } from '@neu_weather_ap/components/charts/bar-chart';
import { WeatherMap } from '@neu_weather_ap/components/map/weather-map';
export const DetailsPage = (props: any) => {
    let activeCity = useSelector((state: NeuWeatherAppState) => state.activeCity)
    let currentLocation: any = useParams()
    const dispatch = useDispatch();
    useEffect(
        () => {
            dispatch(FetchMonthlyForecasts({
                latitude: currentLocation.lat,
                longitude: currentLocation.long
            }));
            let interval = setInterval(
                () => {
                    // dispatch(FetchMonthlyForecasts({
                    //     latitude: currentLocation.lat,
                    //     longitude: currentLocation.long
                    // }));
                }, 5000
            )
            return () => {
                clearInterval(interval)
            }
        }, [currentLocation]
    )

   
    return <Container fluid>
        <div className="details-page m-t-20" >
            <div className="content-wrapper ">
                <div className="main-weather-card">
                    <div className="header-wrapper">
                        <div className="header-container">
                            <div className="today-weather-container">
                                <div className="temp-state-container">
                                    <span className="temperature-text">{activeCity.temp}°</span>
                                    <span className="weather-state-text">{activeCity.weatherText}</span>
                                </div>

                                <div className="hum-wind-container">
                                    <div className="hum-container">
                                        <span className="hum-text">humidity</span>
                                        <span className="hum-value__text">{activeCity.humidity} %</span
                                        >
                                    </div>
                                    <div className="hum-wind-separator">&nbsp;
                                    </div>
                                    <div className="wind-container">
                                        <span className="wind-text">
                                            wind
                                    </span>
                                        <span className="wind-value__text">{activeCity.wind} K/M</span>
                                    </div>
                                </div>
                            </div>
                            <div className="city-name-container">
                                <div className="city-name-underline ">
                                    <div className="city-name-text">
                                        {activeCity.name}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="body-content">
                        <TrackVisibility once>
                            {({ isVisible }) => isVisible && (
                                <Animate
                                    play={true} // set play true to start the animation
                                    duration={1} // how long is the animation duration
                                    delay={2} // how many delay seconds will apply before the animation start
                                    end={{ opacity: 1 }}
                                    start={{ opacity: 0 }}
                                    // complete={{ display: 'none' }}
                                    easeType="cubic-bezier(0.445, 0.05, 0.55, 0.95)"
                                >
                                    <div className="forecast-container">

                                        {
                                            uniqBy(activeCity.monthlyForecasts, "date").map(
                                                (item: any, index) => {
                                                    return (
                                                        <DayWeatherCard key={index} animationDelay={index / 2} day={moment(item.date,"DD/MM/YYYY").format("ddd")}
                                                            icon={item.weatherIcon}
                                                            temperature={item.temp} text={item.weatherText} />)

                                                }
                                            )
                                        }


                                    </div>
                                </Animate>)
                            }</TrackVisibility>

                    </div>
                </div>
            </div>

            <CustomBarChart title="Temperature Forecast(5 days)"
                data={uniqBy(activeCity.monthlyForecasts, "date")}>
                <Bar dataKey="temp" fill="#8884d8" />
                <Bar dataKey="tempMin" fill="#82ca9d" />
                <Bar dataKey="tempMax" fill="#8799d8" />
            </CustomBarChart>

            <CustomLineChart title="Humidity Forecast(5 days)"

                data={uniqBy(activeCity.monthlyForecasts, "date")}>
                <Line dataKey="humidity" fill="#8884d8" />

            </CustomLineChart>

            <span className="m-b-20"></span>
            <div className="chart-container">
                <WeatherMap
                    lat={parseFloat(currentLocation.lat)}
                    long={parseFloat(currentLocation.long)}
                    
                />


               </div>
        </div>

    </Container>
}

