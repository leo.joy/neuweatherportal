import { Container, Row } from 'reactstrap'
import React, { useEffect, useState } from 'react'
import { WeatherCard } from '@neu_weather_ap/components/weather-card'
import { useSelector, useDispatch } from 'react-redux'
import { NeuWeatherAppState } from '@neu_weather_ap/view-models/app-state'
import { NewWeatherCard } from '@neu_weather_ap/components/weather-card/add'
import { push } from 'connected-react-router'
import { CityDetails } from '@neu_weather_ap/view-models/city-model'
import { NewCityModel } from './new'
import { RemoveCityAction } from '@neu_weather_ap/store/actions/weather-card-actions'



export const HomePage = () => {
    let cities = useSelector((state: NeuWeatherAppState) => state.cities)
    const dispatch = useDispatch()
    const [isModalVisble, setModalVisible] = useState(false)
  
    useEffect(
        () => {
            console.log(cities)
        }, [cities]
    )
    const onItemClick = (item: CityDetails) => {

        dispatch(push(`/details/${item.coord.lat}/${item.coord.lon}`));
    }
    const removeItem=(city:CityDetails)=>{
        dispatch(RemoveCityAction(city))
    }
    return <Container>
        <Row className="weather-grid m-t-20">
        <NewWeatherCard onClick={() => { setModalVisible(true) }} className="col-lg-4 m-b-10"></NewWeatherCard>

            {
                cities.map(
                    (city, index) => {
                        return <WeatherCard removeItem={removeItem} onClick={onItemClick} city={city} key={`weathercard-${index}`} title={city.name} className="col-4 m-b-10"></WeatherCard>

                    }
                )
            }
        </Row>
        <NewCityModel isVisible={isModalVisble} setModelState={setModalVisible}/>

    </Container>

}