import { Modal, ModalBody, Col, } from 'reactstrap'
import React, { useEffect, useState, Fragment } from 'react'
import { useDropDownFormInput } from '@neu_weather_ap/components/forms/dropdown'
import { validators } from '@neu_weather_ap/components/forms/validator'
import { places } from '@neu_weather_ap/constants/places'
import "./style.scss";
import { Cordinates, CityDetails } from '@neu_weather_ap/view-models/city-model'
import { getMonthlyForecastData } from '@neu_weather_ap/store/api/weather-api'
import { MapForecastData, getCityDetails } from '@neu_weather_ap/lib/forecast-utils';
import { WeatherCard } from '@neu_weather_ap/components/weather-card'
import { useDispatch } from 'react-redux'
import { AddToCitiesAction } from '@neu_weather_ap/store/actions/weather-card-actions'
import { LocalStorageService } from '@neu_weather_ap/services/local-storage-service'
import { AppConstants } from '@neu_weather_ap/constants/app-contansts'


export const NewCityModel = (props: NewCityProps) => {
    const cityDropDown = useDropDownFormInput("", "city", "city", [validators.requiredValidator("Please select ")])
    const [latlong, setLatlong] = useState<Cordinates>({ lat: "", lon: "" })
    const [city, setCity] = useState<CityDetails>()
    const dispatch = useDispatch()
    const getCityDetailsFromLatLong = (corinates: Cordinates) => {
        getMonthlyForecastData(corinates.lat, corinates.lon).then(
            (details) => {
                let monthly = MapForecastData((details.data.list || []))
                let cityDetails: CityDetails = getCityDetails(details.data)
                cityDetails.monthlyForecasts = monthly
                setCity(cityDetails)
            }
        )

    }
    useEffect(
        () => {
            setLatlong({ lat: "", lon: "" })
            setCity(undefined);
            cityDropDown.setValue(null)
        }, [props.isVisible]
    )
    useEffect(
        () => {
            if (cityDropDown.value) {
                let cordinates: Cordinates = { lon: cityDropDown.value.lng, lat: cityDropDown.value.lat };
                if (latlong.lat != cordinates.lat && latlong.lon != cordinates.lon) {
                    setLatlong(latlong);
                    getCityDetailsFromLatLong(cordinates)
                }
                // if(cityDropDown.lat)
            }
            if (cityDropDown)
                console.log(cityDropDown.value)
        }, [cityDropDown.value]
    )

    const addToCity = () => {
        dispatch(AddToCitiesAction(city));
        if (LocalStorageService.getItems(x => x.lat == city.coord.lat && x.lon == city.coord.lon).length == 0) {
            LocalStorageService.pushItem(AppConstants.cityDataBase, city.coord);
        }
        props.setModelState(false);
    }
    return <Modal backdrop size="lg" isOpen={props.isVisible}  >
        {/* <ModalHeader >Modal title</ModalHeader> */}
        <ModalBody className="add-weather-card">
            <Col>
                <div className="header-popup">Search Cities</div>
                <a className="close-btn" onClick={() => props.setModelState(false)}>X</a>
                {cityDropDown.widget("", "Cities", false, places)}
            </Col>
            <div className="weather-grid">
                {city && <Fragment>
                    <WeatherCard
                        city={city}
                        onClick={() => { }}
                        title={city.name}
                        className="m-r-10" />

                    <button onClick={addToCity} className="add-city-btn">ADD CITY +</button>

                </Fragment>
                }


            </div>
            <div className="m-b-10"></div>
        </ModalBody>


    </Modal >
}
interface NewCityProps {
    isVisible: boolean
    setModelState: Function
}