import { HomePage } from '@neu_weather_ap/pages/home';
import { DetailsPage } from '@neu_weather_ap/pages/details';



export const RouteData = [

    {
        path: "/details/:lat/:long",
        component: DetailsPage,
        isAuthRequired: false,
        showSideBar: true,
        showRouteInSideBar: false,
        title: "Home",
        order: 2,


    },
    {
        path: "/",
        component: HomePage,
        isAuthRequired: false,
        showSideBar: true,
        showRouteInSideBar: false,
        title: "Home",
        order: 1,


    },


]