import React, { useEffect } from 'react';
import { Switch } from 'react-router';

import { Layout } from '../layout';
import { RouteData } from './config';
import { useDispatch } from 'react-redux';
import { FetchCurrentLocationAction, FetchSavedCity } from '@neu_weather_ap/store/actions/app-actions';
// import { LocationService } from '@neu_weather_ap/services/location-service';

const routes = () => {
    const dispatch = useDispatch();
    useEffect(
        () => {
            dispatch(FetchCurrentLocationAction())
            dispatch(FetchSavedCity())
        }, []
    )
    return <div>
        <Switch>
            {
                RouteData.map(
                    (item, index) => {
                        return (
                            <Layout showSidebar={item.showRouteInSideBar} isAuthRequired={item.isAuthRequired} key={`routes${index}`} path={item.path} >
                                <item.component />
                            </Layout>
                        )
                    }
                )
            }
        </Switch>
    </div>
}
export default routes;