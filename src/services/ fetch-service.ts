
import axios, { AxiosRequestConfig, AxiosResponse, AxiosInstance } from "axios";
import { AppConstants } from '@neu_weather_ap/constants/app-contansts';


export class FetchHttpClient {
    ///Single instance for all the requests
    private static instance: AxiosInstance = axios.create(
        {
            //Base Url For Api Services
            baseURL: AppConstants.baseUrl,
            ///Time out 
            timeout: AppConstants.apiTimeOut
        }
    )
    
    static post(
        url: string,
        isAuthenticatedRoot: boolean,
        data: any,
        config?: AxiosRequestConfig
    ): Promise<AxiosResponse> {

        return new Promise<AxiosResponse>((resolve: any, reject: any) => {
            if(isAuthenticatedRoot){

            }
            this.instance.post(url, data, config)
                .then(response => {

                    resolve({ status: response.status, data: response.data });

                })
                .catch(error => {
                    reject(error)
                });
        });
    }
    static put(
        url: string,
        isAuthenticatedRoot: boolean,
        data: any,
        config?: AxiosRequestConfig
    ): Promise<AxiosResponse> {

        return new Promise<AxiosResponse>((resolve: any, reject: any) => {
           
            this.instance.put(url, data, config)
                .then(response => {

                    resolve({ status: response.status, data: response.data });

                })
                .catch(error => {

                    reject(error)
                });
        });
    }
    static get(
        url: string,
        isAuthenticatedRoot: boolean,
        config?: AxiosRequestConfig
    ): Promise<AxiosResponse> {
        return new Promise<AxiosResponse>((resolve: any, reject: any) => {
            let newUrl= url;
            if(isAuthenticatedRoot){
                newUrl+=`&appid=${AppConstants.weatherApiToken}`
            }

            this.instance.get(newUrl, config).then(response => {
                resolve({ status: response.status, data: response.data });

            })
                .catch(error => {

                    reject(error)

                });
        });
    }



}
