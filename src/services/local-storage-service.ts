export class LocalStorageService {

    static pushItem(key: string, item: any) {
        let localstorageData = localStorage.getItem(key);
        if (localstorageData) {
            let parsedData: Array<any> = JSON.parse(localstorageData);
            parsedData.push(item)
            localStorage.setItem(key, JSON.stringify(parsedData))
        }
        else {
            let parsedData = []
            parsedData.push(item)
            localStorage.setItem(key, JSON.stringify(parsedData))
        }

    }
    /// example 
    /// param predicate : x=>x.id==1
    static removeItem(key: any, predicate: any) {
        debugger
        let localstorageData = localStorage.getItem(key);
        if (localstorageData) {
            let parsedData: Array<any> = JSON.parse(localstorageData);
            parsedData=   parsedData.filter(predicate)
            localStorage.setItem(key, JSON.stringify(parsedData))
        }
    }
    static getItems = (key: any, predicate?: any) => {
        debugger
        let localstorageData = localStorage.getItem(key);
        if (localstorageData) {
            let parsedData: Array<any> = JSON.parse(localstorageData);
            if (predicate) {
                parsedData = parsedData.filter(predicate)
            }
            return parsedData
        }
        return []
    }

}