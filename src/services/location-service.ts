import { toast } from 'react-toastify';

export class LocationService {

    ///get current location from browser
    static getCurrentLocation(): Promise<Coordinates> {
        return new Promise(
            (resolve, reject) => {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(
                        (position) => {
                            resolve(position.coords);
                        }, (err) => {
                            console.log(err)
                            toast.warn("This application works better with location service. Please enable location service")
                            resolve(null)
                        }
                    )
                }
            }
        )

    }
}