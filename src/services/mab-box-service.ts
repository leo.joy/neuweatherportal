import { AppConstants } from '@neu_weather_ap/constants/app-contansts';

import axios from "axios";
import { City } from '@neu_weather_ap/view-models/city-model';

export class MapService {

    static getCurrentLocation(cords: Coordinates) {
        return new Promise<City>(
            (resolve, reject) => {
                axios.get(`https://api.mapbox.com/geocoding/v5/mapbox.places/${cords.longitude},${cords.latitude}.json?types=place&access_token=${AppConstants.mapBoxAccessToken}`).then(
                    (res) => {
                        let location =res.data
                        let city:City= {
                            geometry:{
                                longitude:location.query[0],
                                latitude:location.query[1]
                            },
                            placeName:location.features[0].place_name,
                            text:location.features[0].text,
                            id:location.features[0].id
                        }
                        resolve(city)

                    }
                )
            }
        )

    }
}