import { ILoaderWithKey } from '@neu_weather_ap/view-models/loader'
import { ActionTypes } from '@neu_weather_ap/store/actions/action-types'

export const SetLoaders = (loader: ILoaderWithKey) => {
    return {
        type: ActionTypes.SETLOADERS,
        payload: loader
    }
}

export const FetchCurrentLocationAction = () => {
    return {
        type: ActionTypes.FETCH_CURRENT_LOCATION
    }
}
export const FetchMonthlyForecasts = (payload: Partial<Coordinates>) => {
    return {
        type: ActionTypes.FETCH_MONTHLY_FORECASTS,
        payload: payload
    }
}

export const FetchSavedCity=()=>{
    return{
        type:ActionTypes.FETCH_SAVED_CITIES
    }
}