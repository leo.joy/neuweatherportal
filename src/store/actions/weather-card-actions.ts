import { ActionTypes } from '@neu_weather_ap/store/actions/action-types'
import { CityDetails } from '@neu_weather_ap/view-models/city-model'

export const AddToCitiesAction = (payload: CityDetails) => {
    return {
        type: ActionTypes.ADD_TO_CITIES,
        payload: payload
    }
}
export const RemoveCityAction = (payload: CityDetails) => {
    return {
        type: ActionTypes.REMOVE_CITY,
        payload: payload
    }
}