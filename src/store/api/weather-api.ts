import { FetchHttpClient } from '@neu_weather_ap/services/ fetch-service'


export const getWeatherData = (long, lat) => {
    return FetchHttpClient.get(`weather?lat=${lat}&lon=${long}&units=metric`, true)
}
export const getMonthlyForecastData=(lat,long)=>{
    return FetchHttpClient.get(`forecast?lat=${lat}&lon=${long}&units=metric`, true)

}