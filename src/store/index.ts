
import { Store, applyMiddleware, createStore } from 'redux';
import createSagaMiddlware from 'redux-saga';
import {  routerMiddleware } from 'connected-react-router'
import createRootReducer from './reducers';
import { rootSaga } from './saga';
import { NeuWeatherAppState } from '@neu_weather_ap/view-models/app-state';
const sagaMiddleware = createSagaMiddlware();

export function configureStore(history: any, initialState?: NeuWeatherAppState ): Store<NeuWeatherAppState> {

    const middleware = applyMiddleware(
        sagaMiddleware,
        routerMiddleware(history)
    );

    const store = createStore(
        createRootReducer(history),
        initialState,
        middleware
    ) as Store<NeuWeatherAppState>;
    sagaMiddleware.run(rootSaga);
    return store
}