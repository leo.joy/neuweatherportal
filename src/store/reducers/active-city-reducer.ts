import { ActionTypes } from '@neu_weather_ap/store/actions/action-types'
import { ActionType } from '@neu_weather_ap/lib/redux-utils'
import { CityDetails } from '@neu_weather_ap/view-models/city-model'




export const ActiveCityReducer = (state: Partial<CityDetails> = {}, action: ActionType) => {

    switch (action.type) {
        case ActionTypes.SET_ACTIVE_CITY:
            return { ...state, ...action.payload }
        default:
            return state
    }


}