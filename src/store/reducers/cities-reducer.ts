import { ActionTypes } from '@neu_weather_ap/store/actions/action-types'
import { ActionType } from '@neu_weather_ap/lib/redux-utils'
import { CityDetails } from '@neu_weather_ap/view-models/city-model'




export const CitiesReducer = (state: Array<CityDetails> = [], action: ActionType) => {

    switch (action.type) {
        case ActionTypes.FETCH_CURRENT_LOCATION_SUCCESS:
            return [...state, action.payload]
        case ActionTypes.ADD_TO_CITIES:
            {
                if (state.filter(x => x.coord.lat == action.payload.coord.lat && x.coord.lon == action.payload.coord.lon).length == 0) {
                    return [...state, action.payload]
                }
                else {
                    return state
                }
            }
        case ActionTypes.REMOVE_CITY:
            return state.filter(x => x.coord.lat != action.payload.coord.lat && x.coord.lon != action.payload.coord.lon)
        default:
            return state
    }


}