import { combineReducers } from "redux";
import { connectRouter } from 'connected-react-router';
import { LoaderReducer } from './loader-reducer';
import { CitiesReducer } from './cities-reducer';
import { ActiveCityReducer } from './active-city-reducer';


const createRootReducer = (history) => combineReducers<any, any>(
    {
        router: connectRouter(history),
        loaders: LoaderReducer,
        cities: CitiesReducer,
        activeCity: ActiveCityReducer,

    }
)
export default createRootReducer;