import { ActionTypes } from '@neu_weather_ap/store/actions/action-types'
import { ILoaderWithKey } from '@neu_weather_ap/view-models/loader'
import { ActionType } from '@neu_weather_ap/lib/redux-utils'




export const LoaderReducer = (state: Array<ILoaderWithKey> = [], action: ActionType) => {

    switch (action.type) {
        case ActionTypes.SETLOADERS:
            {
                let loaders: Array<ILoaderWithKey> = state || []
                let loader = loaders.find(x => x.key == action.payload.key)
                if (loader) {
                    loaders = loaders.map(
                        x => {
                            if (x.key == action.payload.key) {
                                x.loading = action.payload.loading
                            }
                            return x;
                        }
                    )
                }
                else {
                    loaders.push(action.payload)
                }
                return loaders
            }
        default:
            return state
    }
}