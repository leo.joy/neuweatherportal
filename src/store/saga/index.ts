import { all, takeLatest } from 'redux-saga/effects';
import { ActionTypes } from '@neu_weather_ap/store/actions/action-types';
import * as WeatherSaga from "./weather-saga";
export function* rootSaga() {
    yield all([
        takeLatest(ActionTypes.FETCH_CURRENT_LOCATION, WeatherSaga.getCurrentLocationSaga),
        takeLatest(ActionTypes.FETCH_MONTHLY_FORECASTS, WeatherSaga.getMonthlyForecasts),
        takeLatest(ActionTypes.FETCH_SAVED_CITIES, WeatherSaga.getSavedCities)
    ])
}