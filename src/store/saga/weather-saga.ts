import { LocationService } from '@neu_weather_ap/services/location-service';
// import { MapService } from '@neu_weather_ap/services/mab-box-service';
import { put, call } from 'redux-saga/effects';
import { ActionTypes } from '@neu_weather_ap/store/actions/action-types';
import { CityDetails } from '@neu_weather_ap/view-models/city-model';
import { getWeatherData, getMonthlyForecastData } from '../api/weather-api';
import { MapForecastData, getCityDetails } from '@neu_weather_ap/lib/forecast-utils';
import { LocalStorageService } from '@neu_weather_ap/services/local-storage-service';
import { AppConstants } from '@neu_weather_ap/constants/app-contansts';
export function* getForeCastData(lat, lng) {
    let currentWeather = yield getWeatherData(lng, lat);
    return currentWeather.data
}
export function* getMonthlyForecastsData(latitude, longitude) {
    let monthlyForeCastData = yield call(getMonthlyForecastData, latitude, longitude)
    let monthly = MapForecastData((monthlyForeCastData.data.list || []))
    let cityDetails: CityDetails = getCityDetails(monthlyForeCastData.data)
    cityDetails.monthlyForecasts = monthly
    return cityDetails
}

export function* getCurrentLocationSaga() {

    let cordinates = yield LocationService.getCurrentLocation();
    let cityDetails = yield call(getMonthlyForecastsData, cordinates.latitude, cordinates.longitude);
    if (cityDetails && cityDetails.coord) {
        var items = LocalStorageService.getItems(AppConstants.cityDataBase, x => x.lat == cityDetails.coord.lat && x.lon == cityDetails.coord.lon)
        if (items.length == 0) {

            yield put({
                type: ActionTypes.FETCH_CURRENT_LOCATION_SUCCESS,
                payload: cityDetails
            })
            debugger

            LocalStorageService.pushItem(AppConstants.cityDataBase, cityDetails.coord);
        }
    }



}

export function* getSavedCities() {

    let cityLatlongs: Array<any> = LocalStorageService.getItems(AppConstants.cityDataBase, null)
    for (var item of cityLatlongs) {

        let city = yield getMonthlyForecastsData(item.lat, item.lon);
        yield (
            put(
                {
                    type: ActionTypes.ADD_TO_CITIES,
                    payload: city
                }
            )
        )
    }
    console.log("saved", cityLatlongs)
}

export function* getMonthlyForecasts(action) {
    let cordinates: Coordinates = action.payload;
    let cityDetails = yield call(getMonthlyForecastsData, cordinates.latitude, cordinates.longitude)

    yield put(
        {
            type: ActionTypes.SET_ACTIVE_CITY,
            payload: cityDetails
        }


    )
}