import {  CityDetails } from './city-model';

export interface NeuWeatherAppState {
    cities: Array<CityDetails>
    activeCity:CityDetails
}