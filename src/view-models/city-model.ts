export interface City {
    id: string
    placeName: string,
    text: string,
    geometry: Partial<Coordinates>
    currentWeather?: any
}
export interface Cordinates {
    lat: string,
    lon: string
}
export interface CityDetails {
    name: string,
    coord: Cordinates,
    country: string,
    weatherIcon: string,
    weatherText: string,
    temp: string,
    feelsLike: string,
    tempMin: string,
    tempMax: string,
    pressure: string,
    seaLevel: string,
    grndLevel: string,
    humidity: string,
    wind: string
    monthlyForecasts: Array<any>
}