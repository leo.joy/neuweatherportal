import { LoaderKeys } from 'constants/loading-keys';

export interface ILoaderWithKey {
    loading: boolean
    key: LoaderKeys
}