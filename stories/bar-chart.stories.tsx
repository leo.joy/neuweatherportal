import { storiesOf } from '@storybook/react';
import * as React from 'react';

import { CustomBarChart } from "../src/components/charts/bar-chart"
import { CustomLineChart } from "../src/components/charts/line-chart"

import { Bar, Line } from 'recharts';
import { Container } from 'reactstrap';

storiesOf("Chart", module)
    .add("Bar Chart", () => (
        <Container className="m-t-20">
            <CustomBarChart title={"Bar chart"} data={[
                { "val": 2 },
                { "val": 3 },
                { "val": 4 },
            ]} >

                <Bar fill="lightgreen" dataKey="val"></Bar>
            </CustomBarChart>
            <span>Usage</span>
            <br />
            <code>
                import &#123; CustomBarChart &#125; from '@neu_weather_ap/component/charts/line-chart';<br />
                &lt;CustomBarChart title=&#123; Title of the chart &#125; data=&#123; arrayof data &#125; ><br />
                &lt;Bar fill="strokecolor" dataKey="val">&lt;Bar><br />

                &lt;/CustomBarChart>
                
            </code>
        </Container>
    ))
    .add("Bar Chart Stacked", () => (
        <Container className="m-t-20">

            <CustomBarChart title={"Bar chart"} data={[
                { "val": 2, "val2": 4 },
                { "val": 3, "val2": 4 },
                { "val": 4, "val2": 4 },
            ]} >

                <Bar stroke="#1919192b" dataKey="val" fill="green"></Bar>
                <Bar  stroke="#1919192b" dataKey="val2" fill="blue"></Bar>
            </CustomBarChart>
            <span>Usage</span>
            <br />
            <code>
                import &#123; CustomBarChart &#125; from '@neu_weather_ap/component/charts/line-chart';<br />
                &lt;CustomBarChart title=&#123; Title of the chart &#125; data=&#123; arrayof data &#125; ><br />
                &lt;Bar fill="strokecolor" dataKey="val">&lt;Bar><br />
                &lt;Bar fill="strokecolor2" dataKey="val2">&lt;Bar><br />

                &lt;/CustomBarChart>
                
            </code>
        </Container>
    )).add("Line Chart", () => (
        <Container>

            <CustomLineChart title={"Bar chart"} data={[
                { "val": 2 },
                { "val": 3 },
                { "val": 4 },
            ]} >

                <Line dataKey="val"></Line>
            </CustomLineChart>
            <span>Usage</span>
            <br />
            <code>
                import &#123; CustomLineChart &#125; from '@neu_weather_ap/component/charts/line-chart';<br />
                &lt;CustomLineChart title=&#123; Title of the chart &#125; data=&#123; arrayof data &#125; ><br />
                &lt;Line fill="strokecolor" dataKey="val">&lt;Line><br />
                &lt;/CustomLineChart>

            </code>

        </Container>

    ))