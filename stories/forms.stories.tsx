import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { useDropDownFormInput } from "../src/components/forms/dropdown/index"
import { places } from '../src/constants/places';
import { Container, Row, Col } from 'reactstrap';
storiesOf("Form Control", module).add(
    "DropDown", () => {
        const place = useDropDownFormInput("", "city", "city", [])
        return (
            <React.Fragment>
                <Container className="m-t-20">
                    <Row>
                        <Col lg="4">
                            {place.widget("", "Places", false, places)}
                        </Col>
                    </Row>
                </Container>
                <Container className="m-t-20">
                    <span>Usage</span>
                    <br /> 
                <code>
                 import &#123; useDropDownFormInput &#125; from '"../src/components/forms/dropdown"t';<br /> 

                    /// params: initial value, valueField, textField,validators///<br /> 
                    useDropDownFormInput("", "city", "city", [])<br /> 
                    ///render dropdown<br /> 
                    //param: label,placeHolder,disabled,data <br /> 
                    place.widget("", "Places", false, places)
              </code>
              </Container>
            </React.Fragment>
        )
    }
)