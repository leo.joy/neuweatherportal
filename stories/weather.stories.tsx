import { storiesOf } from '@storybook/react';
import * as React from 'react';

import { WeatherCard } from "../src/components/weather-card";
import { NewWeatherCard } from "../src/components/weather-card/add";
import { WeatherMap } from "../src/components/map/weather-map";
import { Container, Col, Row } from 'reactstrap';
import "leaflet/dist/leaflet.css";
import "bootstrap/dist/css/bootstrap.css";

import "../src/assets/styles/utils.scss"
import "./style.scss"
storiesOf("Weather", module).add(
    "Weather Card", () => {
        return (
            <Container className="m-30">
                <Row>
                    <Col lg="4">
                        <WeatherCard className="" onClick={() => { }} title=""
                            city={
                                {

                                    country: "In",
                                    name: "Your City",
                                    tempMin: "20",
                                    tempMax: "25",
                                    temp: "22",
                                    weatherText: "Clear",
                                    weatherIcon: "50d"
                                }
                            }

                        ></WeatherCard>
                    </Col>
                </Row>
            </Container>
        )
    }
).add(
    "Add Weather Card", () => {
        return (
            <Container className="m-30">
                <Row>
                    <Col lg="4">
                        <NewWeatherCard className="container"

                        ></NewWeatherCard>
                    </Col>
                </Row>
            </Container>
        )
    }
)
    .add(
        " Weather Map", () => {
            return (
                <Container>
                    <WeatherMap lat={9.9833} long={76.2833} />
                </Container>
            )
        }
    )