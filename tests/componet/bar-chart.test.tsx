import React from "react";
import { shallow } from "enzyme";
import { CustomBarChart } from "../../src/components/charts/bar-chart";
import { Bar } from 'recharts';

it("renders the chart", () => {
    const result = shallow(<CustomBarChart data={[]}
        title="Chart"

    >
        <Bar dataKey="city"></Bar>
    </CustomBarChart>)
    result.contains(<div className="city-name-text">Chart</div>);
    // result.find()
    expect(result).toBeTruthy();
});