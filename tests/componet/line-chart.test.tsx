import React from "react";
import { shallow } from "enzyme";
import { CustomLineChart } from "../../src/components/charts/line-chart";
import {  Line } from 'recharts';

it("renders the chart", () => {
    const result = shallow(<CustomLineChart data={[]}
        title="Chart"
    >
        <Line dataKey="city"></Line>
    </CustomLineChart>)
    result.contains(<div className="city-name-text">Chart</div>);
    expect(result).toBeTruthy();
});