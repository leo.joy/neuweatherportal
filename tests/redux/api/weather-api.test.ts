import { WeatherMockData } from '../../fixtures/weather-mock';
import { getMonthlyForecastData, getWeatherData } from '@neu_weather_ap/store/api/weather-api';
describe("Api Tests", () => {
    const genForecastObject = getMonthlyForecastData(WeatherMockData.lat,WeatherMockData.lng);
    const genWeatherObject = getWeatherData(WeatherMockData.lat,WeatherMockData.lng);
    // console.log(genObject)
    it("should return forecast data from api",()=>{
       return genForecastObject.then(
            (res)=>{
                // console.log(res)
                expect(res.status).toEqual(200)
                expect(res.data.list.length).toBeGreaterThan(1)

            }
        )
    })
    it("should return current weather data from api",()=>{
       return genWeatherObject.then(
            (res)=>{
                // console.log(res)
                expect(res.status).toEqual(200)
                expect(res.data.weather.length).toBeGreaterThan(0)

            }
        )
    })
})