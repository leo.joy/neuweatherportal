var webpack = require('webpack');
var path = require('path');

// variables
var isProduction = process.argv.indexOf('-p') >= 0 || process.env.NODE_ENV === 'production';
var sourcePath = path.join(__dirname, './src');
var outPath = path.join(__dirname, './dist');
var postcssImport = require('postcss-import');
var postcssPresetEnv = require('postcss-preset-env');
var cssnano = require('cssnano');
// plugins
var HtmlWebpackPlugin = require('html-webpack-plugin');
var MiniCssExtractPlugin = require('mini-css-extract-plugin');
var WebpackCleanupPlugin = require('webpack-cleanup-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = {
    context: sourcePath,
    entry: {
        app: './main.tsx'
    },
    output: {
        path: outPath,
        filename: 'bundle.js',
        chunkFilename: '[chunkhash].js',
        publicPath: '/'
    },
    target: 'web',
    resolve: {
        extensions: ['.js', '.ts', '.tsx'],
        // Fix webpack's default behavior to not load packages with jsnext:main module
        // (jsnext:main directs not usually distributable es6 format, but es6 sources)
        mainFields: ['module', 'browser', 'main'],
        alias: {

            "@neu_weather_ap/components": path.resolve(__dirname, 'src/components'),
            "@neu_weather_ap/store": path.resolve(__dirname, 'src/store'),
            "@neu_weather_ap/assets": path.resolve(__dirname, 'src/assets'),
            "@neu_weather_ap/lib": path.resolve(__dirname, 'src/lib'),
            "@neu_weather_ap/pages": path.resolve(__dirname, 'src/pages'),
            "@neu_weather_ap/layout": path.resolve(__dirname, 'src/layout'),
            "@neu_weather_ap/saga": path.resolve(__dirname, 'src/store/saga'),
            "@neu_weather_ap/reducers": path.resolve(__dirname, 'src/store/reducers'),
            "@neu_weather_ap/containers": path.resolve(__dirname, 'src/containers'),
            "@neu_weather_ap/view-models": path.resolve(__dirname, 'src/view-models'),
            "@neu_weather_ap/constants": path.resolve(__dirname, 'src/constants'),
            "@neu_weather_ap/services": path.resolve(__dirname, 'src/services')

        }
    },
    module: {
        rules: [
            // .ts, .tsx
            {
                test: /\.tsx?$/,
                use: [

                    !isProduction && {
                        loader: 'babel-loader',
                        options: { plugins: ['react-hot-loader/babel'] }
                    },
                    'ts-loader'
                ],

            },
            {
                test: /\.css$/,
                use: [
                    !isProduction ? 'style-loader' : MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: isProduction,
                            importLoaders: 1,
                        },
                    }, // TODO: enable sourceMap in devMode without FOUC
                    {
                        loader: 'postcss-loader',
                        options: {
                            sourceMap: true,
                            plugins: () => [postcssImport, postcssPresetEnv, cssnano],
                        },
                    },
                ],
            },
            // css
            {
                test: /\.scss$/,
                use: [
                    isProduction ? MiniCssExtractPlugin.loader : "style-loader",

                    {
                        loader: "css-loader" // translates CSS into CommonJS
                    },
                    'resolve-url-loader',
                    {
                        loader: "sass-loader", options: { sourceMap: true }
                    },


                ]
            },
            {
                test: /\.(gif|png|jpe?g|jpg|svg)$/,
                use: 'file-loader'
            },
            
            // static assets
            { test: /\.html$/, use: 'html-loader' },

            { test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/, use: 'file-loader' }
        ]
    },
    optimization: {
        splitChunks: {
            name: true,
            cacheGroups: {
                commons: {
                    chunks: 'all',
                },
                vendors: {
                    test: /[\\/]node_modules[\\/]/,
                    chunks: 'all',
                    priority: 10
                }
            }
        },
        runtimeChunk: true
    },
    plugins: [
        new webpack.EnvironmentPlugin({
            NODE_ENV: isProduction ? 'production' : 'development', // use 'development' unless process.env.NODE_ENV is defined
            DEBUG: false
        }),
        new WebpackCleanupPlugin(),
        new MiniCssExtractPlugin({
            filename: '[contenthash].css',
            disable: !isProduction
        }),
        new HtmlWebpackPlugin({
            template: 'assets/index.html'
        }),
        new CopyWebpackPlugin([
            { from: 'assets/images*', to: outPath }],
            { copyUnmodified: true })

    ],
    devServer: {
        contentBase: sourcePath,
        hot: true,
        inline: true,
        historyApiFallback: {
            disableDotRule: true
        },
        stats: 'minimal'
    },
    node: {
        // workaround for webpack-dev-server issue
        // https://github.com/webpack/webpack-dev-server/issues/60#issuecomment-103411179
        fs: 'empty',
        net: 'empty'
    }
};
